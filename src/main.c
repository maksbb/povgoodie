/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC8xx.h"
#endif

//#include <cr_section_macros.h>
//#include <NXP/crp.h>



// Variable to store CRP value in. Will be placed automatically
// by the linker when "Enable Code Read Protect" selected.
// See crp.h header for more information
//__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

// TODO: insert other include files here
#include "lpc8xx_spi.h"
#include "lpc8xx_gpio.h"
//#include "lpc8xx_mrt.h" //functions not used; only used for defines
#include "../inc/font.h"
#include "../inc/rom_apis.h"
// TODO: insert other definitions and declarations here

/* MRT Control register bit definition. */
#define MRT_INT_ENA					(0x1<<0)
#define MRT_REPEATED_MODE		(0x00<<1)
#define MRT_ONE_SHOT_INT		(0x01<<1)
#define MRT_ONE_SHOT_STALL	(0x02<<1)

/* MRT Status register bit definition */
#define MRT_STAT_IRQ_FLAG		(0x1<<0)
#define MRT_STAT_RUN				(0x1<<1)

/*Filter constants for tilt sensor*/
#define TILT_NUMBER_OF_SAMPLES 4
#define TILT_OFF 0
#define TILT_ON  ((1<<TILT_NUMBER_OF_SAMPLES)-1)

#define MAX_CHARROW 6

#define CHAR_REPETITIONS 1

#ifndef NULL
#define NULL ((void *) 0)
#endif
/*IAP */
#define IAP_LOCATION 0x1fff1ff1


#define PAGENUM(x) (x/64)
#define SECTNUM(x) (x/1024)

enum eIAP_COMMANDS
{
	IAP_PREPARE = 50,
	IAP_COPY_RAM2FLASH,
	IAP_ERASE,
	IAP_BLANK_CHECK,
	IAP_READ_PART_ID,
	IAP_READ_BOOT_VER,
	IAP_COMPARE,
	IAP_REINVOKE_ISP,
	IAP_READ_UID,
	IAP_ERASE_PAGE
};
const uint8_t PoVstring[64] __attribute__ ((section(".PARMPAGE"))) = {'D','i','t',',','d','u','s','^'};

/* UART ROM DRIVER*/
static UART_HANDLE_T *uart;
static uint32_t umem[UART_ROM_MEM_SIZE];
static void rx_callback(uint32_t err_code, uint32_t n);
static void tx_callback(uint32_t err_code, uint32_t n);
static volatile uint8_t byte, test_stop = 0, tx_ok = 0;
/*End of UART ROM DRIVER*/

void MRT_IRQHandler(void);
static void mrt_set_timer(uint8_t, uint32_t);
static void init__mrt(void);
static void SwitchMatrix_Init(void);
static void IOCON_Init(void);
static void SpiSendSingleByte(uint8_t);
static void CopyRam(void *, void *);
static void PoVHandler(void);
static void ZeroMemory(uint8_t *, uint32_t);

volatile uint8_t next_charrow = 0;
volatile uint16_t tiltvalues = 0;
volatile uint8_t msg_received = 0;

static void App_UART_API_Init()
{
	uint32_t frg_mult;
	/* Initialize the UARTs, Get the UART memory size needed */
	volatile uint32_t mem = LPC_UART_API->uart_get_mem_size();
	/* Configure the UART settings */
	UART_CONFIG_T cfg = {0,	/* U_PCLK frequency in Hz */
						 115200,						/* Baud Rate in Hz */
						 1,								/* 8N1 */
						 0,								/* Asynchronous Mode */
						 NO_ERR_EN						/* Enable No Errors */
	};
	cfg.sys_clk_in_hz = 12000000;//Chip_Clock_GetMainClockRate();
	/* Perform a sanity check on the storage allocation */
	if (UART_ROM_MEM_SIZE < (mem / sizeof(uint32_t))) {
		while (1) {}
	}
	/* Setup the UART */
	uart = LPC_UART_API->uart_setup((uint32_t) LPC_USART0, (uint8_t *) umem);

	/* Check the API return value for a valid handle */
	if (uart != NULL) {
		/* initialize the UART with the configuration parameters */
		frg_mult = LPC_UART_API->uart_init(uart, &cfg);	/* API returns FRG MULT for cfg'd baud rate */
		if (frg_mult)
		{
			LPC_SYSCON->UARTFRGDIV = (uint32_t)(0xFF);	/* value 0xFF should be always used */
			LPC_SYSCON->UARTFRGMULT= frg_mult;
		}
	}
}

static void App_Interrupt_Send(const char *send_data, uint32_t length)
{
	UART_PARAM_T param;
	param.buffer = (uint8_t *) send_data;					 //the data to send
	param.size = length;									 //size of the data
	param.transfer_mode = TX_MODE_SZERO_SEND_CRLF;			 //send until zero-terminator of string + append CRLF
	param.driver_mode = DRIVER_MODE_INTERRUPT;				 //Interrupt (non-blocking) transfer
	param.callback_func_pt =(UART_CALLBK_T) tx_callback;	 //call back function when transmit is completed
	 //transmit the message
	//if(
	LPC_UART_API->uart_put_line(uart, &param);
	//) {
	//	App_error_routine();
	//}  ERRORS ARE IGNORED
}



/**
 * @brief	UART1 ISR handler
 * @return	Nothing
 */
void UART0_IRQHandler(void)
{
	LPC_UART_API->uart_isr(uart);
	//SpiSendSingleByte(0x40);
}

int main(void) {
	UART_PARAM_T param;
	const uint8_t msg_ok[] = "OK";
	uint8_t PoVstring_ram[64] __attribute__ ((aligned (64)));
	SystemCoreClockUpdate();
	/* Enable AHB clock to the GPIO domain. */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<6);
	SwitchMatrix_Init();
	IOCON_Init();

	init__mrt();
	mrt_set_timer(0,12000); //1kHz timer
	//mrt_set_timer(1,200000); //5 Hz timer
	mrt_set_timer(2, 70000);   //charrow timer
	/*SPI Init*/
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<11);
	/* Peripheral reset control to SPI, a "1" bring it out of reset. */
	LPC_SYSCON->PRESETCTRL &= ~(0x1<<0);
	LPC_SYSCON->PRESETCTRL |= (0x1<<0);
    LPC_SPI0->DIV = 19;
	LPC_SPI0->DLY = 0;
	LPC_SPI0->CFG = (CFG_ENABLE|CFG_MASTER|CFG_MOSIDRV);
	//SPI_Init(LPC_SPI0,19,(CFG_ENABLE|/*CFG_CPHA|CFG_CPOL|*/CFG_MASTER|CFG_MOSIDRV),0);
	/*USART Init*/
	/* Enable UART clock */
	NVIC_DisableIRQ(UART0_IRQn);
	/* Enable UART clock */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<14);
	/* Peripheral reset control to UART, a "1" bring it out of reset. */
	LPC_SYSCON->PRESETCTRL &= ~(0x1<<3);
	LPC_SYSCON->PRESETCTRL |= (0x1<<3);
	LPC_SYSCON->UARTCLKDIV = 1;

	App_UART_API_Init();
	NVIC_DisableIRQ(UART0_IRQn);
	NVIC_ClearPendingIRQ(UART0_IRQn);
	NVIC_EnableIRQ(UART0_IRQn);
	param.buffer = (uint8_t *) PoVstring_ram;
	param.size = sizeof(PoVstring_ram);
	param.transfer_mode = RX_MODE_LF_RECVD;
	param.driver_mode = DRIVER_MODE_INTERRUPT;
	param.callback_func_pt = (UART_CALLBK_T)rx_callback;
	ZeroMemory(&PoVstring_ram, sizeof(PoVstring_ram)); //fill memory with zeroes
	LPC_UART_API->uart_get_line(uart, &param);         //start interrupt based reception of characters
    while(1)
    {
    	if(msg_received)
    	{
    		App_Interrupt_Send(msg_ok,sizeof(msg_ok));
    		while(!tx_ok);
    		tx_ok = 0;
    		msg_received = 0;
    		SpiSendSingleByte(0x55);

    		CopyRam((void *)PoVstring, (void *)PoVstring_ram);
    		ZeroMemory(&PoVstring_ram, sizeof(PoVstring_ram));
    		LPC_UART_API->uart_get_line(uart, &param);
    	}
    }
    return 0 ;
}

static void rx_callback(uint32_t err_code, uint32_t n)
{
	msg_received = 1;
	SpiSendSingleByte(0xAA);
}

static void tx_callback(uint32_t err_code, uint32_t n)
{
	tx_ok = 1;
}

static void ZeroMemory(uint8_t *ram, uint32_t size)
{
	uint32_t bytecounter;
	for(bytecounter = 0; bytecounter < size ; bytecounter++)
	{
		ram[bytecounter] = 0;
	}
}

static void PoVHandler(void)
{
	static uint8_t PoVstring_index = 0;
	static uint8_t character_repetition_counter = 0;
	static uint8_t PoVstring_charrow = 0;
	static uint8_t one_blank = 0;
	enum tiltstates {DO_WHEN_OFF, WAIT_FOR_REVERSAL, WAIT_FOR_OFF};
	static uint8_t tiltstate = DO_WHEN_OFF;
	const uint8_t msg_blank = "blank";
	switch(tiltstate)
	{
	case DO_WHEN_OFF:
	{
		if(one_blank)
		{
			one_blank = 0;
			SpiSendSingleByte(0);
			tiltstate = WAIT_FOR_REVERSAL;
    		App_Interrupt_Send(msg_blank,sizeof(msg_blank));

		}
		else
		{
			if( (PoVstring[PoVstring_index] != 0x0a) && (PoVstring[PoVstring_index] != 0) && (PoVstring[PoVstring_index] != 0x0D)) //newline or carriage return or zero
			{
				if(PoVstring_index < 64)
				{
					//PoVstring_index = 0;
					//PoVnextLine = 0;
					PoVstring_charrow--;
					SpiSendSingleByte(font_8x8[PoVstring[PoVstring_index]-0x20][PoVstring_charrow]);
					if(!PoVstring_charrow)
					{
						PoVstring_charrow = MAX_CHARROW+1;
						character_repetition_counter++;
						tiltstate = WAIT_FOR_REVERSAL;
					}
					if(character_repetition_counter > CHAR_REPETITIONS)
					{
						one_blank = 1;
						character_repetition_counter = 0;
						PoVstring_index++;
					}

				}
				else
					PoVstring_index = 0;
	    	}
			else
				PoVstring_index = 0;
		}

		break;
	}
	case WAIT_FOR_REVERSAL:
	{
		SpiSendSingleByte(0);
		if(tiltvalues == TILT_ON)
			tiltstate = WAIT_FOR_OFF;
		break;
	}
	case WAIT_FOR_OFF:
	{
		if(tiltvalues == TILT_OFF)
			tiltstate = DO_WHEN_OFF;
		break;
	}
	default:
		break;
	}
}

static void CopyRam(void *flash, void *ram)
{
	unsigned int command_param[5];
	unsigned int status_result[4];
	typedef void (*IAP)(unsigned int [],unsigned int[]);
	IAP iap_entry=(IAP) IAP_LOCATION;
	//while(1);
	NVIC_DisableIRQ(UART0_IRQn);
	NVIC_DisableIRQ(MRT_IRQn);
	// PREPARE SECTOR for WRITE (ADDR/1024)
	command_param[0] = IAP_PREPARE;
	command_param[2] = command_param[1] = SECTNUM((uint32_t)flash);
	command_param[3] = 12000; // NotUsed
	// IAPcommand[4] = 0; // NotUsed
	iap_entry( command_param, status_result );
	if(status_result[0] != 0)
	{
		SpiSendSingleByte(1);
		while(1);
	}
	// ERASE PAGE (ADDR/64)
	command_param[0] = IAP_ERASE_PAGE;
	command_param[2] = command_param[1] = PAGENUM((uint32_t)flash);
	command_param[3] = 12000; // NU12000; //SysCoreClock/1000
	// IAPcommand[4] = 0; // NU
	iap_entry( command_param, status_result );
	if(status_result[0] != 0)
	{
		SpiSendSingleByte(2);
		while(1);
	}
	// PREPARE SECTOR for WRITE (ADDR/1024)
	command_param[0] = IAP_PREPARE;
	command_param[2] = command_param[1] = SECTNUM((uint32_t)flash);
	command_param[3] = 12000; // NotUsed
	// IAPcommand[4] = 0; // NotUsed
	iap_entry( command_param, status_result );
	if(status_result[0] != 0)
	{
		SpiSendSingleByte(3);
		while(1);
	}
		// COPY RAM to FLASH (ADDR/PAGE)
	command_param[0] = IAP_COPY_RAM2FLASH;
	command_param[1] = (uint32_t)flash;
	command_param[2] = (uint32_t)ram;
	command_param[3] = 64; // Write 1 Page
	command_param[4] = 12000;//NotUsed12000; //SysCoreClock/1000
	iap_entry( command_param, status_result );
	if(status_result[0] != 0)
	{
		SpiSendSingleByte(4);
		while(1);
	}
	NVIC_ClearPendingIRQ(UART0_IRQn);
	NVIC_EnableIRQ(UART0_IRQn);
	NVIC_ClearPendingIRQ(MRT_IRQn);
	NVIC_EnableIRQ(MRT_IRQn);
}
static void IOCON_Init() {
	   /* Enable UART clock */
	    LPC_SYSCON->SYSAHBCLKCTRL |= (1<<18);

	    /* Pin I/O Configuration */
	    LPC_IOCON->PIO0_0 = 0xB0;
	    LPC_IOCON->PIO0_1 = 0x80;
	    /* LPC_IOCON->PIO0_2 = 0x90; */
	    /* LPC_IOCON->PIO0_3 = 0x90; */
	    /* LPC_IOCON->PIO0_4 = 0x90; */
	    LPC_IOCON->PIO0_5 = 0x80;
}


static void SwitchMatrix_Init()
{
   /* Enable SWM clock */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<7);

	/* Pin Assign 8 bit Configuration */
	/* U0_TXD */
	/* U0_RXD */
	LPC_SWM->PINASSIGN0 = 0xffff0004UL;
	/* SPI0_SCK */
	LPC_SWM->PINASSIGN3 = 0x02ffffffUL;
	/* SPI0_MOSI */
	/* SPI0_SSEL */
	LPC_SWM->PINASSIGN4 = 0xff01ff05UL;

	/* Pin Assign 1 bit Configuration */
	LPC_SWM->PINENABLE0 = 0xffffffffUL;
}

static void SpiSendSingleByte(uint8_t byte)
{
	LPC_SPI0->TXDATCTL = TXDATCTL_SSELN(0) | TXDATCTL_FSIZE(MASTER_FRAME_SIZE) | TXDATCTL_RX_IGNORE | TXDATCTL_EOT | byte;
}


/******************************************************************************
** Function name:		MRT_IRQHandler
**
** Descriptions:		MRT interrupt handler
**
** parameters:			None
** Returned value:	None
**
******************************************************************************/
void MRT_IRQHandler(void)
{
  if ( LPC_MRT->Channel[0].STAT & MRT_STAT_IRQ_FLAG )
  {
		LPC_MRT->Channel[0].STAT = MRT_STAT_IRQ_FLAG;			/* clear interrupt flag */
		static uint8_t bitpos = 0;
		if(bitpos == TILT_NUMBER_OF_SAMPLES)
			bitpos = 0;
		if(GPIOGetPinValue(PORT0,3))
			tiltvalues |= 1<<bitpos;
		else
			tiltvalues &= ~(1<<bitpos);
		bitpos++;
		//mrt_counter++;
  }
  if ( LPC_MRT->Channel[2].STAT & MRT_STAT_IRQ_FLAG )
  {
		LPC_MRT->Channel[2].STAT = MRT_STAT_IRQ_FLAG;			/* clear interrupt flag */
		next_charrow = 1;
		PoVHandler();
  }
  return;
}

static void mrt_set_timer(uint8_t channel, uint32_t TimerInterval )
{
	//mrt_counter = 0;
	LPC_MRT->Channel[channel].INTVAL = TimerInterval;
	LPC_MRT->Channel[channel].INTVAL |= 0x1UL<<31;
	LPC_MRT->Channel[channel].CTRL = MRT_REPEATED_MODE|MRT_INT_ENA;
}

/******************************************************************************
** Function name:		init_timer
**
** Descriptions:		Initialize timer, set timer interval, reset timer,
**									install timer interrupt handler
**
** parameters:			none
** Returned value:	None
**
******************************************************************************/
static void init__mrt(void)
{
  /* Enable clock to MRT and reset the MRT peripheral */
  LPC_SYSCON->SYSAHBCLKCTRL |= (0x1<<10);
	LPC_SYSCON->PRESETCTRL &= ~(0x1<<7);
	LPC_SYSCON->PRESETCTRL |= (0x1<<7);
  /* Enable the MRT Interrupt */
#if NMI_ENABLED
	NVIC_DisableIRQ( MRT_IRQn );
	NMI_Init( MRT_IRQn );
#else
  NVIC_EnableIRQ(MRT_IRQn);
#endif
  return;
}
